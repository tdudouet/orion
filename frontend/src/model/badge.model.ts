export interface IBadge {
  id: string;
  name: string;
  description: string;
}
