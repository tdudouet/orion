import {IBadge} from "./badge.model";

export interface ICategory {
  id: string;
  name: string;
  description: string;
  badges: IBadge[]
}
