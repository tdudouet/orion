import React, {useEffect, useState} from 'react';
import backend from "./config/backend";
import 'antd/dist/antd.css';
import {Layout} from "antd";
import {ICategory} from "./model/category.model";
import Toolbar from "./components/Toolbar";
import Categories from "./components/Categories";

const {Content} = Layout;

const App: React.FC = () => {

  const [categories, setCategories] = useState<ICategory[]>([]);

  useEffect(() => {
    const fetchCategories = async () => {
      const result = await backend.get('/categories');
      setCategories(result.data);
    };
    fetchCategories();
  }, []);

  return (
    <Layout>
      <Toolbar/>
      <Layout>
        <Content>
          <Categories categories={categories}/>
        </Content>
      </Layout>
    </Layout>
  );
};

export default App;
