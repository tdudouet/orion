import React from "react";
import {IBadge} from "../model/badge.model";
import styled from "styled-components";
import {Col, Row} from "antd";


const StyledBadge = styled("div")`
  {
    width: 350px;
    margin-right: 20px;
    margin-bottom: 20px;
  }
`;

const BadgeLogo = styled("img")`
  {
    width: 100px;
    height: 100px;
    margin-right: 20px;
  }
`;


const Badge: React.FC<{badge: IBadge}> = props => {

  let badgeIcon = require(`../static/badges/${props.badge.id}.png`);
  return (
      <StyledBadge>
        <Row type="flex" align="middle">
          <BadgeLogo src={badgeIcon} alt={props.badge.name}/>
          <Col>
            <strong>{props.badge.name}</strong>
            <br/>
            <span>{props.badge.description}</span>
          </Col>
        </Row>
      </StyledBadge>
    )
};

export default Badge;
