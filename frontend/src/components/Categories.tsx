import React, {Fragment} from "react";
import {ICategory} from "../model/category.model";
import {Card, Row} from "antd";
import Badge from "./Badge";
import {IBadge} from "../model/badge.model";

const Categories: React.FC<{categories: ICategory[]}> = props => {

    return (
      <Fragment>
        {
          props.categories.map((category: ICategory) => (
            <Card title={category.name + ': ' + category.description} key={category.id}>
              <Row type="flex" align="top">
                {
                  category.badges.map((badge: IBadge) => (
                    <Badge badge={badge} key={badge.id}/>
                  ))
                }
              </Row>
            </Card>
          ))
        }
      </Fragment>
    )
};

export default Categories;
