import {Layout} from "antd";
import React from "react";
import styled from "styled-components";

const {Header} = Layout;

const StyledHeader = styled(Header)`
  .header-title {
    color: white;
    font-size: 34px;
  }
`;

const Toolbar: React.FC = () => {

  return (
    <StyledHeader>
      <div className="header-title">Orion</div>
    </StyledHeader>
  )
};

export default Toolbar;
