import axios from 'axios';
import {config} from './config';


let backend = axios.create({
  baseURL: config.api.URL
});

export default backend;
