const local = {
  api: {
    URL: "http://localhost:8080/api"
  }
};

const dev = {
  api: {
    URL: "replace_with_heroku_dev_url"
  }
};

const prod = {
  api: {
    URL: "replace_with_heroku_prod_url"
  }
};

const env = process.env.REACT_APP_ENV;
const envConfig = env === 'local'
  ? local : env === 'dev'
    ? dev
    : prod;

export const config = {
  ...envConfig
};
