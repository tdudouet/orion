package orion.api


import io.micronaut.core.type.Argument
import io.micronaut.http.HttpRequest
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.test.annotation.MicronautTest
import orion.api.model.UserDTO
import orion.model.User
import orion.repository.UserRepository

import javax.inject.Inject

@MicronautTest
class UserApiSpec extends AbstractApiSpec {

    @Inject
    @Client("/")
    RxHttpClient client

    UserRepository repo

    def setup() {
        //
        this.repo = this.getBean(UserRepository)
        //
        this.repo.deleteAll()
    }

    def "There should be two users available from the API"() {
        setup:
        this.repo.save(new User("tdudouet", "tdudouet@sidetrade.com", "Thomas Dudouet"))
        this.repo.save(new User("cdeon", "cdeon@sidetrade.com", "Clément Déon"))

        when:
        def response = this.client.toBlocking().retrieve(HttpRequest.GET("/api/users"), Argument.of(List, UserDTO))

        then:
        //
        response.size() == 2
        //
        UserDTO thomas = response.first()
        thomas.id == "tdudouet"
        thomas.email == "tdudouet@sidetrade.com"
        thomas.name == "Thomas Dudouet"
        //
        UserDTO clement = response.last()
        clement.id == "cdeon"
        clement.email == "cdeon@sidetrade.com"
        clement.name == "Clément Déon"
    }
}
