package orion.api

import io.micronaut.core.type.Argument
import io.micronaut.http.HttpRequest
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.test.annotation.MicronautTest
import orion.api.model.BadgeDTO
import orion.model.Badge
import orion.model.BadgeCategory
import orion.repository.BadgeCategoryRepository
import orion.repository.BadgeRepository

import javax.inject.Inject

@MicronautTest
class BadgeApiSpec extends AbstractApiSpec {

    @Inject
    @Client("/")
    RxHttpClient client

    BadgeCategoryRepository categories
    BadgeRepository badges

    def setup() {
        //
        this.categories = this.getBean(BadgeCategoryRepository)
        this.badges = this.getBean(BadgeRepository)
        //
        this.badges.deleteAll()
        this.categories.deleteAll()
    }

    def "There should be four badges available from the API"() {
        setup:
        //
        def coding = this.categories.save(new BadgeCategory("coding", "Coding", "Coding related stuff"))
        def office = this.categories.save(new BadgeCategory("office", "Office", "Office related stuff"))
        def life = this.categories.save(new BadgeCategory("life", "Life events", "Personal life events"))
        //
        this.badges.save(new Badge("java", "Java coding", "Java coding stuff", coding.id))
        this.badges.save(new Badge("javascript", "Javascript coding", "Javascript coding stuff", coding.id))
        this.badges.save(new Badge("office-closure", "Office closure", "Late bird !", office.id))
        this.badges.save(new Badge("birthday-30", "30 years", "Happy birthday !", life.id))

        when:
        def response = this.client.toBlocking().retrieve(HttpRequest.GET("/api/badges"), Argument.of(List, BadgeDTO))

        then:
        //
        response.size() == 4
        //
        def iterator = response.iterator()
        //
        BadgeDTO java = iterator.next()
        java.id == "java"
        java.name == "Java coding"
        java.description == "Java coding stuff"
        //
        BadgeDTO javascript = iterator.next()
        javascript.id == "javascript"
        javascript.name == "Javascript coding"
        javascript.description == "Javascript coding stuff"
        //
        BadgeDTO officeClosure = iterator.next()
        officeClosure.id == "office-closure"
        officeClosure.name == "Office closure"
        officeClosure.description == "Late bird !"
        //
        BadgeDTO birthday = iterator.next()
        birthday.id == "birthday-30"
        birthday.name == "30 years"
        birthday.description == "Happy birthday !"
    }
}
