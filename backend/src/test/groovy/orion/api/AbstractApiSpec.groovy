package orion.api

import io.micronaut.context.ApplicationContext
import io.micronaut.runtime.server.EmbeddedServer
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification

abstract class AbstractApiSpec extends Specification {

    @Shared
    @AutoCleanup
    EmbeddedServer server = ApplicationContext.run(EmbeddedServer)

    /**
     * Find a Micronaut bean on the target server instance.
     *
     * @param clazz The target bean class
     * @return The Micronaut bean
     */
    protected <T> T getBean(Class<T> clazz) {
        return this.server.getApplicationContext().getBean(clazz)
    }
}
