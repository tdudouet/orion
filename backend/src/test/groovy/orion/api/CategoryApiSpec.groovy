package orion.api

import io.micronaut.core.type.Argument
import io.micronaut.http.HttpRequest
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.test.annotation.MicronautTest
import orion.api.model.BadgeCategoryDTO
import orion.model.Badge
import orion.model.BadgeCategory
import orion.repository.BadgeCategoryRepository
import orion.repository.BadgeRepository

import javax.inject.Inject

@MicronautTest
class CategoryApiSpec extends AbstractApiSpec {

    @Inject
    @Client("/")
    RxHttpClient client

    BadgeCategoryRepository categories
    BadgeRepository badges

    def setup() {
        //
        this.categories = this.getBean(BadgeCategoryRepository)
        this.badges = this.getBean(BadgeRepository)
        //
        this.badges.deleteAll()
        this.categories.deleteAll()
    }

    def "There should be three badge categories available from the API"() {
        setup:
        //
        def _coding = this.categories.save(new BadgeCategory("coding", "Coding", "Coding related stuff"))
        def _office = this.categories.save(new BadgeCategory("office", "Office", "Office related stuff"))
        def _life = this.categories.save(new BadgeCategory("life", "Life events", "Personal life events"))
        //
        this.badges.save(new Badge("java", "Java coding", "Java coding stuff", _coding.id))
        this.badges.save(new Badge("javascript", "Javascript coding", "Javascript coding stuff", _coding.id))
        this.badges.save(new Badge("office-closure", "Office closure", "Late bird !", _office.id))
        this.badges.save(new Badge("birthday-30", "30 years", "Happy birthday !", _life.id))

        when:
        def response = this.client.toBlocking().retrieve(HttpRequest.GET("/api/categories"), Argument.of(List, BadgeCategoryDTO))

        then:
        //
        response.size() == 3
        //
        def iterator = response.iterator()
        //
        BadgeCategoryDTO coding = iterator.next()
        coding.id == "coding"
        coding.name == "Coding"
        coding.description == "Coding related stuff"
        coding.badges.size() == 2
        coding.badges.first().id == "java"
        coding.badges.first().name == "Java coding"
        coding.badges.first().description == "Java coding stuff"
        coding.badges.last().id == "javascript"
        coding.badges.last().name == "Javascript coding"
        coding.badges.last().description == "Javascript coding stuff"
        //
        BadgeCategoryDTO office = iterator.next()
        office.id == "office"
        office.name == "Office"
        office.description == "Office related stuff"
        office.badges.size() == 1
        office.badges.first().id == "office-closure"
        office.badges.first().name == "Office closure"
        office.badges.first().description == "Late bird !"
        //
        BadgeCategoryDTO life = iterator.next()
        life.id == "life"
        life.name == "Life events"
        life.description == "Personal life events"
        life.badges.size() == 1
        life.badges.first().id == "birthday-30"
        life.badges.first().name == "30 years"
        life.badges.first().description == "Happy birthday !"
    }
}
