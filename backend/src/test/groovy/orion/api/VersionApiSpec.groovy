package orion.api

import io.micronaut.http.HttpRequest
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.test.annotation.MicronautTest
import spock.lang.Specification
import spock.lang.Unroll

import javax.inject.Inject

@MicronautTest
class VersionApiSpec extends Specification {

    @Inject
    @Client("/")
    RxHttpClient client

    @Unroll
    def "Dev version should be [#version]"() {
        when:
        def response = this.client.toBlocking().retrieve(HttpRequest.GET("/api/version"), VersionApi.VersionResponse)

        then:
        response.version == version

        where:
        version = "dev (local app)"
    }
}
