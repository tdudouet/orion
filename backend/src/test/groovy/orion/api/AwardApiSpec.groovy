package orion.api

import io.micronaut.core.type.Argument
import io.micronaut.http.HttpRequest
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.test.annotation.MicronautTest
import orion.api.model.AwardDTO
import orion.model.*
import orion.repository.AwardRepository
import orion.repository.BadgeCategoryRepository
import orion.repository.BadgeRepository
import orion.repository.UserRepository
import spock.lang.Unroll

import javax.inject.Inject

@MicronautTest
class AwardApiSpec extends AbstractApiSpec {

    @Inject
    @Client("/")
    RxHttpClient client

    UserRepository users
    BadgeCategoryRepository categories
    BadgeRepository badges
    AwardRepository awards

    def setup() {

        //
        this.users = this.getBean(UserRepository)
        this.categories = this.getBean(BadgeCategoryRepository)
        this.badges = this.getBean(BadgeRepository)
        this.awards = this.getBean(AwardRepository)

        //
        this.awards.deleteAll()
        this.badges.deleteAll()
        this.categories.deleteAll()
        this.users.deleteAll()

        //
        def thomas = this.users.save(new User("tdudouet", "tdudouet@sidetrade.com", "Thomas Dudouet"))
        def clement = this.users.save(new User("cdeon", "cdeon@sidetrade.com", "Clément Déon"))
        //
        def coding = this.categories.save(new BadgeCategory("coding", "Coding", "Coding related stuff"))
        def office = this.categories.save(new BadgeCategory("office", "Office", "Office related stuff"))
        def life = this.categories.save(new BadgeCategory("life", "Life events", "Personal life events"))
        //
        def java = this.badges.save(new Badge("java", "Java coding", "Java coding stuff", coding.id))
        def javascript = this.badges.save(new Badge("javascript", "Javascript coding", "Javascript coding stuff", coding.id))
        def officeClosure = this.badges.save(new Badge("office-closure", "Office closure", "Late bird !", office.id))
        def birthday30 = this.badges.save(new Badge("birthday-30", "30 years", "Happy birthday !", life.id))
        //
        this.awards.save(new Award(thomas.id, java.id, BadgeLevel.GOLD))
        this.awards.save(new Award(thomas.id, javascript.id, BadgeLevel.SILVER))
        this.awards.save(new Award(thomas.id, officeClosure.id, BadgeLevel.BRONZE))
        this.awards.save(new Award(thomas.id, birthday30.id))
        this.awards.save(new Award(clement.id, javascript.id, BadgeLevel.GOLD))
        this.awards.save(new Award(clement.id, officeClosure.id, BadgeLevel.GOLD))
    }

    @Unroll
    def "There should be #count award(s) for user [#user]"() {
        when:
        def response = this.client.toBlocking().retrieve(HttpRequest.GET("/api/awards/users/$user"), Argument.of(List, AwardDTO))

        then:
        response.size() == count

        where:
        user       | count
        'tdudouet' | 4
        'cdeon'    | 2
        'tdu'      | 0
    }

    @Unroll
    def "Awards for user [#user] should be correctly extracted"() {
        when:
        def response = this.client.toBlocking().retrieve(HttpRequest.GET("/api/awards/users/$user"), Argument.of(List, AwardDTO))

        then:
        //
        response.size() == 4
        //
        def it = response.iterator()
        //
        AwardDTO java = it.next()
        java.user.id == "tdudouet"
        java.user.email == "tdudouet@sidetrade.com"
        java.user.name == "Thomas Dudouet"
        java.badge.id == "java"
        java.badge.name == "Java coding"
        java.badge.description == "Java coding stuff"
        java.level == BadgeLevel.GOLD
        //
        AwardDTO javascript = it.next()
        javascript.user.id == "tdudouet"
        javascript.user.email == "tdudouet@sidetrade.com"
        javascript.user.name == "Thomas Dudouet"
        javascript.badge.id == "javascript"
        javascript.badge.name == "Javascript coding"
        javascript.badge.description == "Javascript coding stuff"
        javascript.level == BadgeLevel.SILVER
        //
        AwardDTO officeClosure = it.next()
        officeClosure.user.id == "tdudouet"
        officeClosure.user.email == "tdudouet@sidetrade.com"
        officeClosure.user.name == "Thomas Dudouet"
        officeClosure.badge.id == "office-closure"
        officeClosure.badge.name == "Office closure"
        officeClosure.badge.description == "Late bird !"
        officeClosure.level == BadgeLevel.BRONZE
        //
        AwardDTO birthday30 = it.next()
        birthday30.user.id == "tdudouet"
        birthday30.user.email == "tdudouet@sidetrade.com"
        birthday30.user.name == "Thomas Dudouet"
        birthday30.badge.id == "birthday-30"
        birthday30.badge.name == "30 years"
        birthday30.badge.description == "Happy birthday !"
        birthday30.level == null

        where:
        _ | user
        _ | 'tdudouet'
    }

    @Unroll
    def "There should be #count award(s) for badge [#badge]"() {
        when:
        def response = this.client.toBlocking().retrieve(HttpRequest.GET("/api/awards/badges/$badge"), Argument.of(List, AwardDTO))

        then:
        response.size() == count

        where:
        badge            | count
        'java'           | 1
        'javascript'     | 2
        'office-closure' | 2
        'birthday-30'    | 1
        'birthday-40'    | 0
    }
}
