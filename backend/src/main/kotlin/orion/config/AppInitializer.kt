package orion.config

import io.micronaut.context.annotation.Requires
import io.micronaut.context.env.Environment
import io.micronaut.context.event.ApplicationEventListener
import io.micronaut.discovery.event.ServiceStartedEvent
import orion.model.*
import orion.repository.AwardRepository
import orion.repository.BadgeCategoryRepository
import orion.repository.BadgeRepository
import orion.repository.UserRepository
import javax.inject.Singleton

@Singleton
@Requires(notEnv = [Environment.TEST])
class AppInitializer(
        // Repositories
        private val users: UserRepository,
        private val categories: BadgeCategoryRepository,
        private val badges: BadgeRepository,
        private val awards: AwardRepository
        //
) : ApplicationEventListener<ServiceStartedEvent> {

    override fun onApplicationEvent(event: ServiceStartedEvent) {

        // Add users
        val thomas = this.users.save(User("tdudouet", "tdudouet@sidetrade.com", "Thomas Dudouet"))
        val clement = this.users.save(User("cdeon", "cdeon@sidetrade.com", "Clément Déon"))

        // Add badge categories
        val coding = this.categories.save(BadgeCategory("coding", "Coding", "Anything about coding stuff"))
        val office = this.categories.save(BadgeCategory("office", "Office", "Anything about office stuff"))
        val life = this.categories.save(BadgeCategory("life", "Life events", "Anything about personal life events"))

        // Add badges
        val java = this.badges.save(Badge("java", "Java", "Java coding", coding.id))
        val javascript = this.badges.save(Badge("javascript", "Javascript", "Javascript coding", coding.id))
        val python = this.badges.save(Badge("python", "Python", "Python coding", coding.id))
        val officeOpening = this.badges.save(Badge("office-opening", "Office opening", "Early bird !", office.id))
        val officeClosure = this.badges.save(Badge("office-closure", "Office closure", "Late bird !", office.id))
        val birthday30 = this.badges.save(Badge("30y", "30 years", "30 years ! Happy birthday !", life.id))

        // Add awards (Thomas)
        this.awards.save(Award(thomas.id, java.id, BadgeLevel.GOLD))
        this.awards.save(Award(thomas.id, javascript.id, BadgeLevel.SILVER))
        this.awards.save(Award(thomas.id, python.id, BadgeLevel.BRONZE))
        this.awards.save(Award(thomas.id, officeOpening.id, BadgeLevel.GOLD))
        this.awards.save(Award(thomas.id, officeClosure.id, BadgeLevel.BRONZE))
        this.awards.save(Award(thomas.id, birthday30.id))
        // Add awards (Clément)
        this.awards.save(Award(clement.id, java.id, BadgeLevel.BRONZE))
        this.awards.save(Award(clement.id, javascript.id, BadgeLevel.GOLD))
        this.awards.save(Award(clement.id, python.id, BadgeLevel.SILVER))
        this.awards.save(Award(clement.id, officeClosure.id, BadgeLevel.GOLD))
    }
}