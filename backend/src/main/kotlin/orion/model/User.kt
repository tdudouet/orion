package orion.model

import io.micronaut.data.annotation.DateCreated
import io.micronaut.data.annotation.DateUpdated
import java.time.Instant
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "user_account")
data class User(
        @Id
        val id: String,
        @Column(nullable = false)
        val email: String,
        @Column(nullable = false)
        val name: String
) {
    @DateCreated
    @Column(nullable = false, updatable = false)
    var creationDate: Instant? = null
    @DateUpdated
    @Column(nullable = false)
    var modificationDate: Instant? = null
}