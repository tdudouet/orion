package orion.model

import io.micronaut.data.annotation.DateCreated
import io.micronaut.data.annotation.DateUpdated
import java.time.Instant
import javax.persistence.*

@Entity
data class Award(
        @EmbeddedId
        val id: AwardPk,
        @Column(nullable = true)
        @Enumerated(EnumType.STRING)
        val level: BadgeLevel?
) {
    @DateCreated
    @Column(nullable = false, updatable = false)
    var creationDate: Instant? = null
    @DateUpdated
    @Column(nullable = false)
    var modificationDate: Instant? = null

    constructor(userId: String, badgeId: String, level: BadgeLevel) : this(AwardPk(userId, badgeId), level)

    constructor(userId: String, badgeId: String) : this(AwardPk(userId, badgeId), null)
}