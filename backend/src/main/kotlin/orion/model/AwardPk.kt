package orion.model

import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
data class AwardPk(
        @Column(name = "user_id", nullable = false)
        val userId: String,
        @Column(name = "badge_id", nullable = false)
        val badgeId: String
)