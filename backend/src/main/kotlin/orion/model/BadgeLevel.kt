package orion.model

enum class BadgeLevel(val rank: Int) {
    BRONZE(3),
    SILVER(2),
    GOLD(1)
}