package orion.repository

import io.micronaut.data.jdbc.annotation.JdbcRepository
import io.micronaut.data.repository.CrudRepository
import orion.model.BadgeCategory

@JdbcRepository
interface BadgeCategoryRepository : CrudRepository<BadgeCategory, String>