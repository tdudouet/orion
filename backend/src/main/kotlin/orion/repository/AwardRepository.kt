package orion.repository

import io.micronaut.data.jdbc.annotation.JdbcRepository
import io.micronaut.data.repository.CrudRepository
import orion.model.Award
import orion.model.AwardPk

@JdbcRepository
interface AwardRepository : CrudRepository<Award, AwardPk>