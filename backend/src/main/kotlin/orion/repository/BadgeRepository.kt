package orion.repository

import io.micronaut.data.jdbc.annotation.JdbcRepository
import io.micronaut.data.repository.CrudRepository
import orion.model.Badge

@JdbcRepository
interface BadgeRepository : CrudRepository<Badge, String> {

    /**
     * Find badges for a given category.
     *
     * @param id The target category id
     * @return The corresponding badges
     */
    fun findByCategoryId(id: String): List<Badge>
}