package orion.api.model

import orion.model.Badge
import orion.model.BadgeCategory

data class BadgeCategoryDTO(
        val id: String,
        val name: String,
        val description: String,
        val badges: List<BadgeDTO>
) {
    constructor(category: BadgeCategory, badges: List<Badge>) : this(category.id, category.name, category.description, badges.map { BadgeDTO(it) })
}