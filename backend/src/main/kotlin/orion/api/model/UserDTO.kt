package orion.api.model

import orion.model.User

data class UserDTO(
        val id: String,
        val email: String,
        val name: String
) {
    constructor(user: User) : this(user.id, user.email, user.name)
}