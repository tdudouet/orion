package orion.api.model

import orion.model.Badge
import orion.model.BadgeLevel
import orion.model.User

data class AwardDTO(
        val user: UserDTO,
        val badge: BadgeDTO,
        val level: BadgeLevel?
) {
    constructor(user: User, badge: Badge, level: BadgeLevel?) : this(UserDTO(user), BadgeDTO(badge), level)
}