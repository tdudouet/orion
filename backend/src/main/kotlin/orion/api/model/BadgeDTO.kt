package orion.api.model

import orion.model.Badge

data class BadgeDTO(
        val id: String,
        val name: String,
        val description: String
) {
    constructor(badge: Badge) : this(badge.id, badge.name, badge.description)
}