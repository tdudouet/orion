package orion.api

import orion.api.model.BadgeCategoryDTO
import orion.repository.BadgeCategoryRepository

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import orion.repository.BadgeRepository

@Controller("/api/categories")
class CategoryApi(private val badgeCategoryRepo: BadgeCategoryRepository, private val badgeRepo: BadgeRepository) {

    @Get
    fun getCategories(): Collection<BadgeCategoryDTO> {
        return this.badgeCategoryRepo.findAll().map { BadgeCategoryDTO(it, this.badgeRepo.findByCategoryId(it.id)) }
    }
}
