package orion.api

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import orion.api.model.UserDTO
import orion.repository.UserRepository

@Controller("/api/users")
class UserApi(private val repo: UserRepository) {

    @Get
    fun getUsers(): Collection<UserDTO> {
        return this.repo.findAll().map { UserDTO(it) }
    }
}
