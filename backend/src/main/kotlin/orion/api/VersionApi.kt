package orion.api

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import orion.Application

@Controller("/api/version")
class VersionApi {

    @Get
    fun getVersion(): VersionResponse {
        return VersionResponse(Application.javaClass.`package`.implementationVersion ?: "dev (local app)")
    }

    //

    data class VersionResponse(val version: String)
}
