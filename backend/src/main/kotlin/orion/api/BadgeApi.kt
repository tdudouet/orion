package orion.api

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import orion.api.model.BadgeDTO
import orion.repository.BadgeRepository

@Controller("/api/badges")
class BadgeApi(private val badges: BadgeRepository) {

    @Get
    fun getBadges(): Collection<BadgeDTO> {
        return this.badges.findAll().map { BadgeDTO(it) }
    }
}
