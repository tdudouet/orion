package orion.api

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import orion.api.model.AwardDTO
import orion.model.Award
import orion.repository.AwardRepository
import orion.repository.BadgeRepository
import orion.repository.UserRepository

@Controller("/api/awards")
class AwardApi(
        private val awards: AwardRepository,
        private val badges: BadgeRepository,
        private val users: UserRepository
) {

    @Get("/users/{id}")
    fun getAwardsByUser(@PathVariable id: String): Collection<AwardDTO> {
        return this.awards.findAll().filter { it.id.userId == id }.map { this.map(it) }
    }

    @Get("/badges/{id}")
    fun getAwardsByBadge(@PathVariable id: String): Collection<AwardDTO> {
        return this.awards.findAll().filter { it.id.badgeId == id }.map { this.map(it) }
    }

    //

    private fun map(award: Award): AwardDTO {
        //
        val user = this.users.findById(award.id.userId).orElseThrow()
        val badge = this.badges.findById(award.id.badgeId).orElseThrow()
        //
        return AwardDTO(user, badge, award.level)
    }
}
