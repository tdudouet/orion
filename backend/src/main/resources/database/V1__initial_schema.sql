-- UserAccount model
CREATE TABLE user_account (
    id VARCHAR(255) NOT NULL PRIMARY KEY,
    email VARCHAR(255) NOT NULL,
    name VARCHAR(255) NOT NULL,
    creation_date TIMESTAMP WITH TIME ZONE NOT NULL,
    modification_date TIMESTAMP WITH TIME ZONE NOT NULL
);

-- BadgeCategory model
CREATE TABLE badge_category (
    id VARCHAR(255) NOT NULL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    description VARCHAR(255) NOT NULL,
    creation_date TIMESTAMP WITH TIME ZONE NOT NULL,
    modification_date TIMESTAMP WITH TIME ZONE NOT NULL
);

-- Badge model
CREATE TABLE badge (
    id VARCHAR(255) NOT NULL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    description VARCHAR(255) NOT NULL,
    category_id VARCHAR(255) NOT NULL REFERENCES badge_category(id),
    creation_date TIMESTAMP WITH TIME ZONE NOT NULL,
    modification_date TIMESTAMP WITH TIME ZONE NOT NULL
);

-- Award model
CREATE TABLE award (
    user_id VARCHAR(255) NOT NULL REFERENCES user_account(id),
    badge_id VARCHAR(255) NOT NULL REFERENCES badge(id),
    level VARCHAR(255),
    creation_date TIMESTAMP WITH TIME ZONE NOT NULL,
    modification_date TIMESTAMP WITH TIME ZONE NOT NULL,
    PRIMARY KEY (user_id, badge_id)
);